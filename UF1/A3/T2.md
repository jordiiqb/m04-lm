Utilitzar documentació interactiva
==================================

MP4UF1A3T2

Estudi del vocabulari HTML en base a la documentació en línia

Documentació del vocabulari HTML
--------------------------------

-   El DTD de qualsevol vocabulari XML serveix com descripció formal
    pels programes i com a documentació per a les persones.
-   Normalment cal completar el DTD amb un document de referència, per
    us exclusiu de les persones.
-   El document oficial del [W3C](http://www.w3c.org) es massa formal.
-   El document creat pel [Web Design Group](http://htmlhelp.com/) és
    rigorós però simple.
-   Cal fugir de les documentacions de Microsoft (en ocasions presents
    en documents “contaminats” d’altres).

Orientacions per l’estudi del vocabulari HTML
---------------------------------------------

-   El document HTML es divideix en dos grans blocs: element `head`, amb
    diversa metainformació, i l’element `body`, amb el document
    a mostrar.
-   Els elements interior a l’element `body` son de tipus *bloc* o
    *inline*.
-   Els elements de tipus bloc introdueixen sempre un nou paràgraf, amb
    salt de línia.
-   Els elements de tipus inline determinen característiques del text.

Aspectes a seguir però als que no obliga el DTD {#ASPECTES}
-----------------------------------------------

Els elements des de &lt;h1&gt; fins a &lt;h6&gt; serveixen per
estructurar el text en seccions i subseccions, però com que no
s’escriuen niuats uns dins dels altres, som nosaltres els que amb
disciplina hem de seguir aquestes normes:

-   És costum usar un sol element &lt;h1&gt; en cada document, amb els
    mateix text que l’element &lt;title&gt;, però res impedeix en el DTD
    fer-ho de forma diferent.
-   Un element &lt;h2&gt; ha d’estar precedit d’un element &lt;h1&gt;,
    l’element &lt;h3&gt; de l’element &lt;h2&gt;, etc. No hem
    fer “salts”.
-   Tot el text que hi ha a continuació d’un d’aquests elements, fins
    arribar a un de nivell igual o superior, està *conceptualment*
    dins seu.
-   Mai triem aquests elements per com es *mostren* visualment, sinó pel
    seu paper en organitzar la jerarquia conceptual del text.

Enllaços recomanats
-------------------

-   [WDG HTML Help](http://htmlhelp.com/)
-   [WDG HTML 4.0
    Reference](http://htmlhelp.com/distribution/wdghtml40.tar.gz)
    ([local](aux/wdghtml40.tar.gz))
-   [XHTML Cheat
    Sheet](http://www.cheat-sheets.org/saved-copy/htmlcheatsheet.pdf)
    ([local](aux/htmlcheatsheet.pdf))

**Molt important:** porta impresa a classe la XHTML Cheat Sheet.

Pràctiques I
------------

Pràctiques sobre la documentació del WDG.

-   Instal·la (com administrador) a `/usr/local/doc` la referència HTML
    del WDG.
-   En la secció de bookmarks del navegador fes una carpeta de nom
    Biblioteca per documentacions locals.
-   Recorda adaptar mentalment a XHTML el que digui la documentació
    (feta originalment per HTML), i amaga sempre els elements
    no estrictes.
-   Estudia el possible contingut de l’element `head`, i millora si cal
    la plantilla que tens per crear nous fitxers HTML.

Pràctiques II
-------------

Creació de documents XHTML.

-   En tot moment consulta la documentació (local) del [WDG HTML
    Help](http://htmlhelp.com/)
-   En base a la [plantilla](T1.html#TEMPLATE) creada en dies anteriors
    crea un nou document XHTML.
-   Per validar el teu document has de fer servir aquesta ordre:
    `xmllint --noout --valid`.
-   Com a text pel teu document fes servir fragments en
    [llatí](http://lipsum.com/). D’aquesta forma no et distreurà el
    significat del text (tradicionalment els grafistes ho fan així).
-   En aquest document construeix l’estructura general del mateix segons
    el DTD del XHTML estricte i en base als elements `h1`, `h2`, `h3`,
    `p`, `ul`, `ol`, `ul` i `dl`.
-   Respecta les [recomanacions](#ASPECTES) d’estil comentades
    més amunt.
-   Afegeix al teu document enllaços interns i externs com ara aquests:
    1.  Al peu de la pàgina un enllaç al seu [inici](#).
    2.  Un [enllaç](#ASPECTES) a un element de la mateixa pàgina
        identificat pel seu atribut `id`.
    3.  Un [enllaç](#HTMLHELP) a un element `a` de la mateixa pàgina
        identificat pel seu atribut `name`.
    4.  Un [enllaç](T1.html) a una pàgina diferent situada en el
        mateix directori.
    5.  Un [enllaç](T1.html#TEMPLATE) a una secció d’una pàgina diferent
        situada en el mateix directori.
    6.  Un [enllaç](http://informatica.escoladeltreball.org/ASIX/MP4/) a
        una pàgina qualsevol d’Internet.

