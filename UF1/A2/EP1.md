Creació de nous DTDs
====================

MP4UF1A2EP1

Exercici pràctic (EP1)

Característiques de l’exercici
------------------------------

Exercici pràctic, amb activitats com ara:

-   Donat un document XML, dissenyar el seu DTD.
-   Donat un DTD, preparar un document de prova que sigui vàlid respecte
    el DTD.
-   Donada una especificació de dades, dissenyar un DTD i un document
    de prova.

Durant l’exercici hi ha llibertat per consultar documentació disponible
localment. No es disposarà d’accés a Internet.

### Enunciat

El dia de realització de l’exercici el seu enunciat estarà disponible en
format XHTML en el [repositori local](_EP1.html) de fitxers.

### Criteri de qualificació

L’exercici aporta el 50% de la nota del resultat d’aprenentatge associat
a l’activitat.
