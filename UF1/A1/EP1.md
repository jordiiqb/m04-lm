Creació de documents XML
========================

MP4UF1A1EP1

Exercici pràctic (EP1)

Característiques de l’exercici
------------------------------

Al llarg del curs els exercicis pràctics seran un instrument avaluació
habitual. Aquestes proves, similars a les activitats pràctiques
realitzades en general, sempre plantegen problemes reals com els que un
administrador de sistemes es pot trobar en la seva activitat diària.

Els exercicis pràctics sempre sempre es faran utilitzant d’ordinadors,
amb llibertat per consultar documentació disponible localment. En moltes
ocasions, com en la d’aquest exercici particular, no es disposarà
d’accés a Internet.

### Tipus d’exercici

Aquest exercici servirà per mesurar les habilitats en l’edició de
documents XML ben formats.

### Enunciat

El dia de realització de l’exercici el seu enunciat estarà disponible en
format XHTML en el [repositori local](_EP1.html) de fitxers.

### Criteri de qualificació

L’exercici aporta el 50% de la nota del resultat d’aprenentatge associat
a l’activitat.
