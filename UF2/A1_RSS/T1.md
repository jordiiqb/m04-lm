Utilitzar lectors i agregadors RSS
==================================

MP4UF2A1T1

Eines per consulta de canals web (*feeds*)

Canals web (punt de vista de l’usuari)
--------------------------------------

Els [canals web](aux/pub-rss.jpg) són percebuts pels usuaris finals en
un nivell d’abstracció molt alt, essent la tecnologia subjacent gairebé
invisible.

-   Utilitzant agregadors l’usuari pot confeccionar les seves
    combinacions de noticies.
-   L’usuari pot visitar pàgines amb canals agregats sense identificar
    les fonts reals de cada informació.
-   Els *bloggers* poden oferir als seus lectors la possibilitat de
    subscriure’s a canals web.
-   Els *bloggers* poden enriquir els seus blogs amb dades
    proporcionades per canals web d’altres blogs.

Enllaços recomanats
-------------------

-   [Digg Reader](http://digg.com/reader)
-   [Flipboard](https://flipboard.com/)
-   [Linux Feed Reader](http://liferea.sourceforge.net/)
-   [Barrapunto](http://barrapunto.com/)
-   [Slashdot](http://slashdot.org/)
-   [La Vanguardia](http://www.lavanguardia.com/rss)

Pràctiques
----------

-   Utilitza l’agregador de Digg per unir diferents canals RSS.
-   Configura diferents canals RSS amb el programa Linux Feed Reader
    (instal·la el paquet `liferea` de Fedora, i afegeix canals copiant
    l’adreça dels canals).
-   Explora el servei de *Live Bookmarks* del navegador Firefox (per
    exemple amb aquest
    [*feed*](https://sites.google.com/a/correu.escoladeltreball.org/fedora/faq/posts.xml)).
-   Si tens un dispositiu Android pots instal·lar algun agregador de RSS
    com ara la *app* de Digg. En el repositori lliure
    [F-Droid](https://f-droid.org/) tens més d’un, com ara
    [sparserss](https://code.google.com/p/sparserss/).

